# ArgoCD

## Install ArgoCD CLI

```bash
brew install argocd
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
kubectl port-forward svc/argocd-server -n argocd 8080:443
```

## Namespaces

```bash
kubectl create -n dev
kubectl create -n prod
k9s -n <NAMESPACE>
```

## Get Credentials dashboard

```yaml
username: admin
password: in_the_next_step
```

## Generate password

```bash
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
```

## Port-Forward app

```bash
kubectl port-forward -n dev service/myhelmapp 8888:80
kubectl port-forward -n prod service/myhelmapp 8888:80
```

## ADD app

```txt
GENERAL
- ++NEW APP
- NAME
- PROJECT NAME
- SYNC POLICY
- AUTO-CREATE NAMESPACE

SOURCE
- REPOISTORY-URL
- REVISION(BRANCH)
- PATH(LOCATION OF THE CHART YAML FILE)

DESTINATION
- CLUSTER-URL
- NAMESPACE

HELM(AUTODETECT)
- VALUES-FILES(CONFIG SPECIFIC DEPLOY TEST, PROD, STAGING)
- EXTRA PARAMETERS
```
